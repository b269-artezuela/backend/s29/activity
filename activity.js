

// 2.
db.users.find(
	{
		$or : [
			{
				firstName: {$regex: 'S',$options: '$i'}
			},
			{
				lastName: {$regex: 'D',$options: '$i'}
			}
			]
	

	},
	{	firstName: 1,
		lastName: 1,
		_id: 0}
		)

//3.
db.users.find({$and : [{age: {$gte:70}},{department: "HR"}]})



//4. 
db.users.find({$and : [{age: {$lte:30}},{firstName: {$regex: 'E',$options: '$i'}}]})